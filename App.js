/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


import {createAppContainer} from 'react-navigation';

import Login from './screens/login';
import Order from './screens/order';
import OrderDetails from './screens/orderDetails';

const Stack = createStackNavigator();

const MyStack = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false }} >
        <Stack.Screen
          name="Home"
          component={Login}/>
        <Stack.Screen name="order" component={Order} />
        <Stack.Screen name="orderdetails" component={OrderDetails} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default class App extends Component {
  render() {
    return (
      <MyStack />


    );
  }
}


