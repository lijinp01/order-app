/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { StackNavigator} from 'react-navigation';
import { NavigationContainer } from '@react-navigation/native';
import { NavigationActions } from 'react-navigation';
import {NativeModules} from 'react-native';
import {
    StyleSheet,
    View,
    Text,
    Button,
    TextInput,
    TouchableOpacity,
    Alert,
} from 'react-native';
import Order from '../login';
import { CalendarScreen } from 'react-native-scrollable-tab-view';
//const { navigation } = this.props;


export default class LoginForm extends Component {

    state = { username: "", password: "" }
    checkLogin() {
        const { username, password } = this.state;

        fetch('http://woba.getautospare.com/api/shop/login', {
            method: 'POST', headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }, body: JSON.stringify({ password: 'SF2h75TJ', user_name: 'mufeedsoftfruit@gmail.com' }),
        })
            .then(res => {
                return res.json();
            })
            .then(res => {
                if (res.status === 1) {
                    Alert.alert('Correct', 'valid input', [{ text: 'OK' }]);
                   // this.navigation.navigate('orderdetails');
                } else {
                    Alert.alert('Incorrect', 'invalid credentials', [{ text: 'Try Again' }]);
                }
            })
    }
    render() {
        return (
            <View style={styles.loginform}>
                <TextInput
                    style={styles.textBox}
                    placeholder="Username"
                    underlineColorAndroid="rgba(0,0,0,0)"
                    onSubmitEditing={() => this.password.focus()}
                    onChangeText={text => this.setState({ username: text })}
                />
                <TextInput
                    style={styles.textBox}
                    placeholder="Password"
                    underlineColorAndroid="rgba(0,0,0,0)"
                    secureTextEntry={true}
                    ref={(input) => this.password = input}
                    onChangeText={text => this.setState({ password: text })}
                />
                <TouchableOpacity style={styles.button} onPress={_ => this.checkLogin()}>
                    <Text style={styles.loginButton} >Login</Text>
                </TouchableOpacity>

                {/* <Button
                    title="OrderPageee" 
                    onPress={() => this.navigation.navigate('orderdetails')}
                /> */}


            </View>
        );
    }
}
const styles = StyleSheet.create({
    loginform: {
        flex: 2,
        alignItems: 'center',
        marginTop: 40,
    },
    textBox: {
        height: 40,
        width: 200,
        borderRadius: 20,
        borderColor: 'gray',
        borderWidth: 1,
        paddingHorizontal: 20,
        marginVertical: 5,
    },
    loginButton: {
        fontSize: 16,
        fontWeight: '500',
        color: 'white',
        alignItems: 'center',
    },
    button: {
        width: 150,
        backgroundColor: 'green',
        height: 40,
        borderRadius: 10,
        marginTop: 15,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
