/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    StatusBar,
} from 'react-native';


export default class LoginPageLogo extends Component {

    render() {
        //const [value, onChangeText] = React.useState('Useless Placeholder');
        return (
            <>

                <View style={styles.logo}>
                    
                    <Image
                        source={require('../../asset/logo.jpg')}
                        style={{ width: 220, height: 100 }}
                    />

                </View>
            </>
        );
    }
}
const styles = StyleSheet.create({
    logo: {
        flex: 1.5,
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginBottom: 20,
    },
});
