/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    Alert,
    TouchableOpacity,
    StatusBar,
} from 'react-native';
import LoginPageLogo from './components/loginPageLogo';
export default class Login extends Component {
    state = { username: "", password: "" }
    checkLogin() {
        const { username, password } = this.state;

        fetch('http://woba.getautospare.com/api/shop/login', {
            method: 'POST', headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }, body: JSON.stringify({ password: password, user_name: username }),
        })
            .then(res => {
                return res.json();
            })
            .then(res => {
                if (res.status === 1) {
                    //Alert.alert('Correct', 'valid input', [{ text: res.token }]);
                   this.props.navigation.navigate('order',{ token: res.token });
                } else {
                    Alert.alert('Incorrect', 'invalid credentials', [{ text: 'Try Again' }]);
                }
            })
    }

    render() {
        return (
                <View style={styles.container}>
                    <StatusBar
                        backgroundColor="green"
                        barStyle="dark-content"
                    />
                <LoginPageLogo/>
                <View style={styles.loginform}>
                <TextInput
                    style={styles.textBox}
                    placeholder="Username"
                    underlineColorAndroid="rgba(0,0,0,0)"
                    onSubmitEditing={() => this.password.focus()}
                    onChangeText={text => this.setState({ username: text })}
                />
                <TextInput
                    style={styles.textBox}
                    placeholder="Password"
                    underlineColorAndroid="rgba(0,0,0,0)"
                    secureTextEntry={true}
                    ref={(input) => this.password = input}
                    onChangeText={text => this.setState({ password: text })}
                />
                <TouchableOpacity style={styles.button} onPress={_ => this.checkLogin()}>
                    <Text style={styles.loginButton} >Login</Text>
                </TouchableOpacity>

                


            </View>
                
                </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      },
      loginform: {
        flex: 2,
        alignItems: 'center',
        marginTop: 40,
    },
    textBox: {
        height: 40,
        width: 200,
        borderRadius: 20,
        borderColor: 'gray',
        borderWidth: 1,
        paddingHorizontal: 20,
        marginVertical: 5,
    },
    loginButton: {
        fontSize: 16,
        fontWeight: '500',
        color: 'white',
        alignItems: 'center',
    },
    button: {
        width: 150,
        backgroundColor: 'green',
        height: 40,
        borderRadius: 10,
        marginTop: 15,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
