/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    View,
    Text,
    FlatList,
    Image,
    Button,
    StyleSheet,
    StatusBar,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
//import Icon from 'react-native-vector-icons/FontAwesome';

const PendingOrder = [
    {
        id: "01",
        itemName: "Chappal W107",
        image: "https://upload.wikimedia.org/wikipedia/commons/7/76/Peshawari_Chappal_Charsadda_Style.jpg",
        size: "7",
        quantity: "5"
    },
];
const CompletedOrder = [
    {
        id: "01",
        itemName: "Chappal W109",
        image: "https://i.ebayimg.com/images/g/PhkAAOSwiHZbIW69/s-l400.jpg",
        size: "7",
        quantity: "5",
    }, {
        id: "03",

        itemName: "Chappal W103",
        image: "https://5.imimg.com/data5/JC/XM/MY-14706280/pishori-sandal-500x500.jpg",
        size: "7",
        quantity: "5",
    },
];


//http://www.json-generator.com/api/json/get/cghCFcPGRe?indent=2

const Item = ({ title }) => (
    <View style={styles.item}>
        <Image style={styles.img} source={{ uri: title.images[0].picture }} />
        <View style={{ flex: 1, flexDirection: 'column', paddingLeft: 30, }}>
            <Text style={styles.title}>Chappal W10{title.product_id}</Text>
            <Text style={styles.details}>Size : {title.variant.size}</Text>
            <Text style={styles.details}>Quantity : {title.quantity}</Text>
        </View>

        <Icon name="arrow-right" size={30} color={"green"} style={{ paddingRight: 20, }} />
    </View>
);
const ItemPending = ({ title, size, image, quantity }) => (
    <View style={styles.item}>
        <View style={{ height: 100, width: 8, backgroundColor: 'red', marginTop: 1 }}></View>
        <Image style={styles.img} source={{ uri: image }} />
        <View style={{ flex: 1, flexDirection: 'column', paddingLeft: 30, }}>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.details}>Size : {size}</Text>
            <Text style={styles.details}>Quantity : {quantity}</Text>
        </View>
        <Icon name="arrow-right" size={30} color={"green"} />
    </View>
);
const ItemComplete = ({ title, size, image, quantity }) => (
    <View style={styles.item}>
        <View style={{ height: 100, width: 8, backgroundColor: 'green', marginTop: 1 }}></View>
        <Image style={styles.img} source={{ uri: image }} />
        <View style={{ flex: 1, flexDirection: 'column', paddingLeft: 30, }}>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.details}>Size : {size}</Text>
            <Text style={styles.details}>Quantity : {quantity}</Text>
        </View>
        <Icon name="arrow-right" size={30} color={"green"} />
    </View>
);

export default class Order extends Component {

    onNextClick() {
        var tocken = this.props.route.params.token;
        this.props.navigation.navigate('orderdetails',{ token: tocken });
        console.warn(tocken);
    }


    constructor() {
        // const {state} = this.props.navigation;
        // console.log("tocken " + state.params.tocken);
        super();
        this.state = {
            data: [],
        };
    }

    componentDidMount() {
        var tocken = this.props.route.params.token;
        tocken = 'Bearer ' + tocken;

        fetch('http://woba.getautospare.com/api/shop/orders', {
            method: 'GET', headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': tocken,
            },
        })
            .then(res =>
                res.json())
            .then(resJson => {
                this.setState({
                    data: resJson.orders[0],
                });
            })
            .catch((error) => {
                console.log(error);
            });
    }
    render() {
        const renderItem = ({ item }) => (
            <Item title={item} />
        );
        // const {state} = this.props.navigation;
        const renderItemPending = ({ item }) => (
            <ItemPending title={item.itemName} size={item.size} image={item.image} quantity={item.quantity} />
        );
        const renderItemComplete = ({ item }) => (
            <ItemComplete title={item.itemName} size={item.size} image={item.image} quantity={item.quantity} />
        );
        return (
            <>
                <ScrollView>
                    <View>
                        <StatusBar
                            backgroundColor="green"
                            barStyle="dark-content"
                        />
                        <Button title="GOTO Order details" color="green" onPress={_ => this.onNextClick()} />
                        <View style={{ height: 35, flexDirection: 'row', paddingHorizontal: 20, justifyContent: 'space-between' }}>
                            <Icon name="window-minimize" size={20} color={"black"} />
                            <Icon name="user-circle" size={30} color={"black"} />

                        </View>

                        <Text style={styles.sectionHeader}>New Orders</Text>
                    </View>
                    <View style={styles.container}>
                        <FlatList
                            data={this.state.data.products}
                            renderItem={renderItem}
                            keyExtractor={item => item.id}
                        />
                    </View>
                    <View>
                        <Text style={styles.sectionHeader}>Accepted Orders</Text>
                    </View>
                    <View style={{ backgroundColor: "white" }}>
                        <View style={styles.container2}>
                            <Text style={styles.subheading}>PendingOrder</Text>
                            <FlatList
                                data={PendingOrder}
                                renderItem={renderItemPending}
                                keyExtractor={item => item.id}
                            />
                            <Text style={styles.subheading}>Completed Order</Text>
                            <FlatList
                                data={CompletedOrder}
                                renderItem={renderItemComplete}
                                keyExtractor={item => item.id}
                            />
                        </View>
                    </View>
                </ScrollView>
            </>

        );
    }


}

const styles = StyleSheet.create({
    container: {
        paddingTop: 10,
        backgroundColor: 'white',
    }, container2: {
        paddingTop: 10,
        marginVertical: 5,
        borderRadius: 10,
        marginHorizontal: 20,
        elevation: 4,
        backgroundColor: 'white',
    },
    sectionHeader: {
        paddingTop: 10,
        paddingLeft: 20,
        paddingRight: 10,
        fontSize: 18,
        color: 'grey',
        fontWeight: '600',
        backgroundColor: 'white',
    },
    subheading: {
        paddingLeft: 20,
        paddingRight: 10,
        fontSize: 14,
        color: 'grey',
        fontWeight: '600',
        backgroundColor: 'white',
    },
    item: {
        padding: 10,
        marginVertical: 5,
        borderRadius: 10,
        marginHorizontal: 20,
        elevation: 1.5,
        flexDirection: 'row',
        fontSize: 18,
        alignItems: 'center',
        height: 100,
    },
    img: {
        width: 80,
        height: 80,
    },
    title: {
        fontSize: 16,
        fontWeight: '700',
        backgroundColor: 'white',
    },
    details: {
        fontSize: 16,
        fontWeight: '400',
        color: 'grey',
        backgroundColor: 'white',
    }
});