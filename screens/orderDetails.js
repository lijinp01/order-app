/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    View,
    Text,
    FlatList,
    Image,
    StyleSheet,
    Switch,
    StatusBar,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
const ItemPending = ({ title }) => (
    <View style={styles.item}>
        <Image style={styles.img} source={{ uri: title.images[0].picture }} />
        <View style={{ flexDirection: 'column', paddingLeft: 30, }}>
            <Text style={styles.title}>Chappal W10{title.product_id}</Text>
            <Text style={styles.details}>Size : {title.variant.size}</Text>
            <Text style={styles.details}>Quantity : {title.quantity}</Text>
        </View>
    </View>
);

export default class Order extends Component {
    constructor() {
        super();
        this.state = {
            datap: [],
            data: [],
            customer: [],
        };
    }


    componentDidMount() {
        var tocken = this.props.route.params.token;
        tocken = 'Bearer ' + tocken;
        fetch('http://woba.getautospare.com/api/shop/order-details', {
            method: 'POST', headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': tocken,
            }, body: JSON.stringify({ order_id: '2'}),
        })
            .then(res =>
                res.json())
            .then(resJson => {
                this.setState({
                    customer: resJson.order.billing_address,
                });
            })
            .catch((error) => {
                console.log(error);
            });
             fetch('http://woba.getautospare.com/api/shop/order-details', {
            method: 'POST', headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': tocken,
            }, body: JSON.stringify({ order_id: '2'}),
        })
            .then(res =>
                res.json())
            .then(resJson => {
                this.setState({
                    data: resJson.order,
                });
            })
            .catch((error) => {
                console.log(error);
            });
            fetch('http://woba.getautospare.com/api/shop/order-details', {
                method: 'POST', headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': tocken,
                }, body: JSON.stringify({ order_id: '2'}),
            })
                .then(res =>
                    res.json())
                .then(resJson => {
                    this.setState({
                        datap: resJson.order.products,
                    });
                })
                .catch((error) => {
                    console.log(error);
                });


        
        
    }
    render() {
        const renderItem = ({ item }) => (
            <ItemPending title={item} />
        );
        //const [isEnabled, setIsEnabled] = useState(false);
        //const toggleSwitch = () => setIsEnabled(previousState => !previousState);
        return (
            <>
                <ScrollView style={styles.container}>
                    <StatusBar
                        backgroundColor="green"
                        barStyle="dark-content"
                    />
                    <View style={{ height: 35, flexDirection: 'row', paddingHorizontal: 20, justifyContent: 'space-between' }}>
                        <Icon name="window-minimize" size={20} color={"black"} />
                        <Icon name="user-circle" size={30} color={"black"} />

                    </View>
                    <Text style={styles.sectionHeader}>Order Details</Text>
                    <FlatList
                        data={this.state.datap}
                        renderItem={renderItem}
                        keyExtractor={item => item.id}
                    />
                    <Text style={styles.sectionHeader}>Customer Details</Text>
                    <View style={styles.customer}>
                        <View style={{ flex: 1, flexDirection: 'column', paddingLeft: 30 }}>
                            <Text style={styles.title}><Icon name="user" size={30} color={"green"} />   Name</Text>
                            <Text style={styles.title}><Icon name="address-card" size={22} color={"green"} />  Adress</Text>
                            <Text style={styles.title}><Icon name="plane" size={30} color={"green"} />  Landmark</Text>
                            <Text style={styles.title}><Icon name="phone" size={30} color={"green"} />  Phone</Text>

                        </View>

                        <View style={{ flex: 1, flexDirection: 'column', paddingTop: 10 }}>
                            <Text style={styles.customerValue}>{this.state.data.customer_name}</Text>
                            <Text style={styles.customerValue}>{this.state.customer.city}</Text>
                            <Text style={styles.customerValue}>{this.state.customer.landmark}</Text>
                            <Text style={styles.customerValue}>{this.state.customer.phone}</Text>

                        </View>
                    </View>

                    <Text style={styles.sectionHeader}>Payment Details</Text>
                    <View style={styles.paymentDetails}>
                        <View style={{ flex: 1, flexDirection: 'column', paddingLeft: 10 }}>
                            <Text style={{ fontWeight: "700", paddingBottom: 10 }}>Cash paid</Text>
                            <Text style={{ fontWeight: "700" }}>Cash to be recieved</Text>

                        </View>
                        <View style={{ flex: 1, flexDirection: 'column' }}>
                            <Text style={{ paddingBottom: 10, color: 'green' }} >₹{this.state.data.discount}</Text>
                            <Text style={{ color: 'red' }} >₹{this.state.data.amount}</Text>

                        </View>

                    </View>
                    <View style={{ height: 35, flexDirection: 'row', paddingHorizontal: 25, justifyContent: 'space-between',marginBottom:20, }}>
                        <Text style={{fontSize:19}}>Mark As Delivered</Text>
                        <Switch
                        trackColor={{ false: "#767577", true: "lightgreen" }}
                        thumbColor={ "green"}
                        ios_backgroundColor="#3e3e3e"
                        //onValueChange={toggleSwitch}
                        value={true}
                    />

                    </View>
                    

                </ScrollView>

            </>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 10,
        backgroundColor: 'white',
    },
    sectionHeader: {
        paddingTop: 10,
        paddingLeft: 20,
        paddingRight: 10,
        fontSize: 18,
        color: 'grey',
        fontWeight: '600',
        backgroundColor: 'white',
    },
    item: {
        padding: 10,
        marginVertical: 5,
        borderRadius: 10,
        marginHorizontal: 20,
        elevation: 1.5,
        flexDirection: 'row',
        fontSize: 18,
        alignItems: 'center',
        height: 100,
    },
    customer: {
        padding: 10,
        marginVertical: 5,
        borderRadius: 10,
        marginHorizontal: 20,
        elevation: 1.5,
        flexDirection: 'row',
        fontSize: 18,
        alignItems: 'center',
        justifyContent: 'center',

    },
    customerDetails: {
        padding: 10,
        marginVertical: 5,
        borderRadius: 10,
        marginHorizontal: 20,
        elevation: 1.5,
        flexDirection: 'row',
        fontSize: 18,
        alignItems: 'center',


    },
    customerValue: {
        paddingVertical: 11,
    },
    paymentDetails: {
        padding: 10,
        marginVertical: 5,
        borderRadius: 10,
        marginHorizontal: 20,
        flexDirection: 'row',
        elevation: 1.5,
        fontSize: 18,
        alignItems: 'center',
        marginBottom: 20,
    },
    img: {
        width: 90,
        height: 90,
    },
    title: {
        fontSize: 16,
        paddingVertical: 6,
        fontWeight: '700',
        backgroundColor: 'white',

    },
    details: {
        fontSize: 16,
        fontWeight: '400',
        color: 'grey',
        backgroundColor: 'white',
    },
});